const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ImageMinimizerPlugin = require("image-minimizer-webpack-plugin");

module.exports = (env) => ({
    entry: "./src/index.js",
    output: {
        filename: "main.[contenthash].js",
    },
    module: {
        rules: [
         {
           test: /\.png$/i,
           type: 'asset/resource',
         },
         {
            test: /\.scss$/i,
            use: [
              env.prod ? MiniCssExtractPlugin.loader :'style-loader',
              'css-loader',
              'sass-loader',
            ],
          },
          {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: [
                  ['@babel/preset-env']
                ]
              }
            }
          }
        ],
    },
    plugins: [
        new HtmlWebpackPlugin( {template: 'index.html'} ), 
        new MiniCssExtractPlugin({
            filename: 'main.[contenthash].css',
        })],
    devServer:{
            historyApiFallback: true,
            hot: true
        },
    optimization: {
        minimizer: [
            "...",
            new ImageMinimizerPlugin({
            minimizer: {
                implementation: ImageMinimizerPlugin.imageminMinify,
                options: {
                    plugins: [
                        ["optipng", { optimizationLevel: 5 }],
                    ]
                },
            },
            }),
        ],
        },
})